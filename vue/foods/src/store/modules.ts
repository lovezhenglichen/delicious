import user from "./modules/user"
import cart from "./modules/cart"
import app from "./modules/app";

const modules = {
  user,
  cart,
  app,
}

export {
  modules
};
