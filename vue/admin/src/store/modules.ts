import user from "./modules/user"
import menu from "./modules/menu"
import app from "./modules/app";

const modules = {
  user,
  menu,
  app,
}

export {
  modules
};
