# 操作指南

​	[前台地址:http://food.f1dao.cn](http://food.f1dao.cn)

​	[后台地址:http://f-admin.f1dao.cn](http://f-admin.f1dao.cn)

1. mysql创建数据库delicious,字符集:utf8mb4,排序规则:utf8mb4_0900_ai_ci,并导入数据脚本,在sql目录下delicious.sql文件.

2. 运行认证服务器token-authorization

3. 运行资源服务器provider

4. vue文件下右键打开终端,切换到到admin目录下

5. 使用命令

   ```npm
   // 下载vue相应框架组件(注意在admin目录下运行命令)
   npm install --legacy-peer-deps
   // 启动vue项目
   npm run dev
   ```

6. 登录
   
- 账户:user, 密码: user(密码就是用户名)
   
7. 